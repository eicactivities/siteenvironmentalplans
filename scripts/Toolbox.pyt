import arcpy, os
import ExportMap

current_path = os.path.dirname(__file__)


def get_feature_set_from_json(json_string):
    try:
        return arcpy.AsShape(json_string, True)
    except:
        return None

class Toolbox(object):
    def __init__(self):
        """Define the toolbox (the name of the toolbox is the name of the
        .pyt file)."""
        self.label = "Toolbox"
        self.alias = ""

        # List of tool classes associated with this toolbox
        self.tools = [Tool]


class Tool(object):
    def __init__(self):
        """Define the tool (tool name is the name of the class)."""
        self.label = "Tool"
        self.description = ""
        self.canRunInBackground = False

    def getParameterInfo(self):
        """Parameter definitions"""
        #
        param_extent = arcpy.Parameter(
            displayName="Input Extent",
            name="in_extent",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        param_markup_polygons = arcpy.Parameter(
            displayName="Markup Polygons",
            name="markup_polygons",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        param_markup_polylines = arcpy.Parameter(
            displayName="Markup Polylines",
            name="markup_polylines",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        param_markup_points = arcpy.Parameter(
            displayName="Markup Points",
            name="markup_points",
            datatype="GPString",
            parameterType="Required",
            direction="Input")
        # Output PDF file
        param_out = arcpy.Parameter(
            displayName="Output Message",
            name="out_message",
            datatype="DEFile",
            parameterType="Derived",
            direction="Output")
        params = [param_extent, param_markup_polygons, param_markup_polylines, param_markup_points, param_out]
        return params

    def isLicensed(self):
        """Set whether tool is licensed to execute."""
        return True

    def updateParameters(self, parameters):
        """Modify the values and properties of parameters before internal
        validation is performed.  This method is called whenever a parameter
        has been changed."""
        return

    def updateMessages(self, parameters):
        """Modify the messages created by internal validation for each tool
        parameter.  This method is called after internal validation."""
        return

    def execute(self, parameters, messages):
        """The source code of the tool."""
        scratch = arcpy.env.scratchWorkspace
        if not scratch:
            return
            #scratch = 'C' + ':/' + 'Temp' # have to try to trick AGS to not recognise this as a path, or it uploads every GDB in c:/Temp on Hudson into the deployment!!!
        source_mxd = os.path.join(current_path, 'SEP_Edit.mxd')
        target_pdf = os.path.join(scratch,
                              'SEP_Edit' + '.' + 'pdf')  # Don't know why I have to hide some of these paths from AGS,but this combination works...
        extent_list = parameters[0].value.split(" ")
        extent = arcpy.Extent(float(extent_list[0]), float(extent_list[1]), float(extent_list[2]), float(extent_list[3]))
        #with open(os.path.join(current_path, "log.txt"), "w") as f:
        #    f.write(feature_set_as_json)
        ExportMap.export_map(source_mxd, target_pdf, extent, get_feature_set_from_json(parameters[1].value), \
                             get_feature_set_from_json(parameters[2].value), get_feature_set_from_json(parameters[3].value))
        parameters[4].value = target_pdf
        return

