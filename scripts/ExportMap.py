import arcpy, os

current_path = os.path.dirname(__file__)
polygons_markup_layer_name = "markup_" + "polygons" # have to trick AGS so it doesn't try to refactor this string during publishing
polylines_markup_layer_name = "markup_" + "polylines" # have to trick AGS so it doesn't try to refactor this string during publishing
points_markup_layer_name = "markup_" + "points" # have to trick AGS so it doesn't try to refactor this string during publishing

def export_map(source_mxd, target_pdf, extent, feature_set_polygons, feature_set_polylines, feature_set_points):
    current_mxd = arcpy.mapping.MapDocument(source_mxd)
    if feature_set_polygons != None:
        arcpy.MakeFeatureLayer_management(feature_set_polygons, polygons_markup_layer_name)
        addLayer = arcpy.mapping.Layer(polygons_markup_layer_name)
        arcpy.mapping.AddLayer(current_mxd.activeDataFrame, addLayer)
    if feature_set_polylines != None:
        arcpy.MakeFeatureLayer_management(feature_set_polylines, polylines_markup_layer_name)
        addLayer = arcpy.mapping.Layer(polylines_markup_layer_name)
        arcpy.mapping.AddLayer(current_mxd.activeDataFrame, addLayer)
    if feature_set_points != None:
        arcpy.MakeFeatureLayer_management(feature_set_points, points_markup_layer_name)
        addLayer = arcpy.mapping.Layer(points_markup_layer_name)
        arcpy.mapping.AddLayer(current_mxd.activeDataFrame, addLayer)
    current_mxd.activeDataFrame.extent = extent
    arcpy.RefreshActiveView()
    arcpy.mapping.ExportToPDF(current_mxd, target_pdf)
