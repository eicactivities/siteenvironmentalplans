'''
inputs:
project name
symbology mxd
templates
'''

import os, datetime, argparse, shutil, arcpy
import utils_arcgis_server, utils_geocortex
from distutils.dir_util import copy_tree

current_path = os.path.dirname(__file__)

project_name = 'Dev'
mapservice_name = 'SEP_' + project_name
if project_name != 'Dev':
    gpservice_name = 'ExportMap'
else:
    gpservice_name = 'ExportMap_Dev'

ags_keyword_mapserver = 'MapServer'
in_mxd_path = os.path.abspath(os.path.join(current_path, '..', 'mxd', mapservice_name + '.mxd'))
sde_connection_path = os.path.abspath(os.path.join(current_path, '..', 'config', 'Connection to dbserver01 - TEST.sde'))
ags_connection_path = os.path.abspath(os.path.join(current_path, '..', 'config', 'rmacaw on gis.construction.ags'))
sep_mapservice_folder_name = 'SiteEnvironmentalPlans'
geocortex_site_name_base = 'SiteEnvironmentalPlans'
target_arcgisserver_input_dir = '//gisserver01/arcgisserver/directories/arcgissystem/arcgisinput'
target_geocortex_sites_dir = '//webserver01/REST Elements/Sites'
schema_gdb = os.path.abspath(os.path.join(current_path, '..', 'config', 'schema.gdb'))


def setup_working_directory():
    working_dir = os.path.abspath(os.path.join(current_path, '..', 'working', datetime.datetime.today().strftime('%Y%m%d%H%M%S')))
    os.makedirs(working_dir)
    return working_dir

def create_schema_in_sde(schema_gdb, sde_connection_path):
    old_workspace = arcpy.env.workspace
    arcpy.env.workspace = schema_gdb

    for fc_name in arcpy.ListFeatureClasses('*'):
        source_fc = os.path.join(schema_gdb, fc_name)
        spatial_reference = arcpy.Describe(source_fc).spatialReference
        target_fc = os.path.join(sde_connection_path, fc_name)
        if not arcpy.Exists(target_fc):
            arcpy.CreateFeatureclass_management(sde_connection_path, fc_name, None, source_fc, spatial_reference=spatial_reference)
            print "Created " + target_fc
    arcpy.env.workspace = old_workspace

def repoint_mxd(in_mxd_path, target_sde_connection, repointed_mxd_path):
    utils_arcgis_server.repoint_mxd(in_mxd_path, target_sde_connection, repointed_mxd_path)

def apply_mapservice_permissions(service_folder, mapservice_name, service_type, admin_user, admin_password):
    '''
    Sets a role's permission for ArcGIS Server geometry service.
    --admin_user       Publisher/administrator user to log into ArcGIS Server with.
    --admin_password   Password for publisher/administrator login.
    --server           Server machine.  Optional, default is localhost.
    --port             Port to use when connecting.  Option, default 6080.
    --role             ArcGIS Server role being affected.
    --secure           Requires a secure login.
    '''
    role = r'eic\MWC_Editor'
    serverName = "gisserver01"
    serverPort = 6080
    secure = False
    parameters = (service_folder, mapservice_name, service_type, admin_user, admin_password, serverName, serverPort, role, secure)

    utils_arcgis_server.apply_mapservice_permissions(parameters)

def publish_mapservice(repointed_mxd_path, ags_connection_path, working_directory):
    parameters = {utils_arcgis_server.parameter_name_folder: sep_mapservice_folder_name, utils_arcgis_server.parameter_name_mapservice: mapservice_name}
    utils_arcgis_server.publish_mapservice(repointed_mxd_path, ags_connection_path, working_directory, parameters)

def publish_gpservice(ags_connection_path, working_directory, template_mxd):
    parameters = {utils_arcgis_server.parameter_name_folder: sep_mapservice_folder_name, utils_arcgis_server.parameter_name_gpservice: gpservice_name}
    utils_arcgis_server.publish_gpservice(ags_connection_path, working_directory, parameters)

    # Copy up mxd templates because I can't work out how to get AGS deployment process to recognise them
    shutil.copyfile(template_mxd, os.path.join(target_arcgisserver_input_dir, \
        sep_mapservice_folder_name, gpservice_name + '.GPServer/extracted/v101/scripts', 'SEP_Edit.mxd'))

def publish_site(mapservice_folder_name, mapservice_name):

    source_site = os.path.abspath(os.path.join(current_path, '../sites', geocortex_site_name_base))
    target_site_name = geocortex_site_name_base + '_' + project_name
    temp_working_path = os.path.join(working_directory, 'site', target_site_name)
    target_site = os.path.join(target_geocortex_sites_dir, target_site_name)
    copy_tree(source_site, temp_working_path)
    utils_geocortex.rename_site(temp_working_path, target_site_name)
    utils_geocortex.repoint_mapservices(temp_working_path, mapservice_folder_name, mapservice_name)
    copy_tree(temp_working_path, target_site)

# -------------------------------------------------------------------------------------------------------------------- #
# Main sequence
parser = argparse.ArgumentParser()
parser.add_argument("admin_user")
parser.add_argument("admin_password")
args = parser.parse_args()


###########################################################
# set up working directory
working_directory = setup_working_directory()

###########################################################
# add ad role/group

###########################################################
# setup sde
create_schema_in_sde(schema_gdb, sde_connection_path)

###########################################################
# repoint mxds to sde
repointed_mxd_path = os.path.join(working_directory, os.path.basename(in_mxd_path).split('.')[0] + '_repointed.mxd')
repoint_mxd(in_mxd_path, sde_connection_path, repointed_mxd_path)

# ###########################################################
# # publish mapservice including featureservice
# publish_mapservice(repointed_mxd_path, ags_connection_path, working_directory)
#
# ###########################################################
# # apply permissions to mapservice
# apply_mapservice_permissions(sep_mapservice_folder_name, mapservice_name, ags_keyword_mapserver, args.admin_user, \
#                              args.admin_password)

###########################################################
# publish geoprocessing service
publish_gpservice(ags_connection_path, working_directory, repointed_mxd_path)

# ###########################################################
# # publish site
# publish_site(sep_mapservice_folder_name, mapservice_name)

###########################################################
# apply/update permissions in site
