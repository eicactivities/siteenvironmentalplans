import os
from xml.etree import ElementTree
from collections import OrderedDict

def site_file_path(site_path):
    return os.path.join(site_path, "Site.xml")

def xml_file_to_element_tree(site_path):
    tree = ElementTree.ElementTree()
    root = tree.parse(site_file_path(site_path), OrderedDict())
    return tree, root

def rename_site(site_path, target_site_name):
    tree, root = xml_file_to_element_tree(site_path)
    # Set site name in Site element
    root.set('ID', target_site_name)
    root.set('DisplayName', target_site_name)

    tree.write(site_file_path(site_path), encoding='UTF-8')

def repoint_mapservices(site_path, mapservice_folder_name, mapservice_name):
    tree, root = xml_file_to_element_tree(site_path)

    succeeded = False
    for element in root.findall("Map/MapServices/FeatureLayer"):
        connection_string = element.get('ConnectionString')
        if 'SEP' in connection_string and 'FeatureServer' in connection_string:
            excision_start = connection_string.index('arcgis/rest/services/') + len('arcgis/rest/services/')
            excision_end = connection_string.index('/FeatureServer')
            replacement_string = connection_string[0:excision_start] + mapservice_folder_name + '/' +  mapservice_name+ connection_string[excision_end:]
            element.set('ConnectionString', replacement_string)
            succeeded = True
    if not succeeded: raise Exception("Failed to find FeatureLayer elements with SEP FeatureServices in {0}").format(site_file_path(site_path))

    tree.write(site_file_path(site_path), encoding='UTF-8')
