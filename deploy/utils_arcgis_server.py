# Publishes a service to machine myserver using USA.mxd
# A connection to ArcGIS Server must be established in the
#  Catalog window of ArcMap before running this script
import arcpy, os, json, shutil
from xml.etree import ElementTree
from collections import OrderedDict
import httplib, urllib  # used for connecting to ArcGIS Server
import re  # used for parsing responses

current_path = os.path.dirname(__file__)
parameter_name_folder = 'folder'
parameter_name_mapservice = 'mapservice'
parameter_name_gpservice = 'gpservice'

def repoint_mxd(in_mxd_path, to_workspace, out_mxd_path):
    mxd = arcpy.mapping.MapDocument(in_mxd_path)
    mxd.replaceWorkspaces('', '', to_workspace, 'SDE_WORKSPACE')
    mxd.saveACopy(out_mxd_path)
    del mxd

def publish_mapservice(in_mxd_path, ags_connection_path, working_directory, parameters):

    # Set up service sources and outputs
    mapDoc = arcpy.mapping.MapDocument(in_mxd_path)
    service = parameters[parameter_name_mapservice]
    sddraft = os.path.join(working_directory, service + '.sddraft')
    sddraft_backup = os.path.join(working_directory, service + '.backup')
    sd = os.path.join(working_directory, service + '.sd')
    summary = 'Site Environmental Plans'
    tags = 'SEP'

    # Create service definition draft
    arcpy.mapping.CreateMapSDDraft(mapDoc, sddraft, service, 'ARCGIS_SERVER', ags_connection_path, True, parameters[parameter_name_folder], summary, tags)
    apply_sddraft_changes(sddraft)
    shutil.copyfile(sddraft, sddraft_backup)
    # Analyze the service definition draft
    analysis = arcpy.mapping.AnalyzeForSD(sddraft)

    # Print errors, warnings, and messages returned from the analysis
    print "The following information was returned during analysis of the MXD:"
    for key in ('messages', 'warnings', 'errors'):
      print '----' + key.upper() + '---'
      vars = analysis[key]
      for ((message, code), layerlist) in vars.iteritems():
        print '    ', message, ' (CODE %i)' % code
        print '       applies to:',
        for layer in layerlist:
            print layer.name,
        print

    # Stage and upload the service if the sddraft analysis did not contain errors
    if analysis['errors'] == {}:
        # Execute StageService. This creates the service definition.
        arcpy.StageService_server(sddraft, sd)

        # Execute UploadServiceDefinition. This uploads the service definition and publishes the service.
        arcpy.UploadServiceDefinition_server(sd, ags_connection_path)
        print "Service successfully published"
    else:
        raise Exception("Service could not be published because errors were found during analysis.")

    print arcpy.GetMessages()

def apply_sddraft_changes(sddraft_file_path):
    tree = ElementTree.ElementTree()
    root = tree.parse(sddraft_file_path, OrderedDict())

    # FeatureServer
    succeeded = False
    for element in root.findall("Configurations/SVCConfiguration/Definition/Extensions/SVCExtension"):
        if element.find('TypeName').text == 'FeatureServer':
            element.find('Enabled').text = 'true'
            succeeded = True
    if not succeeded: raise Exception("Failed to find FeatureServer element in {0}").format(sddraft_file_path)

    # Put the xmlns atts back in - for some reason ElementTree ignores them on import!
    root.set('xmlns:xs' ,'http://www.w3.org/2001/XMLSchema')
    root.set('xmlns:typens', 'http://www.esri.com/schemas/ArcGIS/10.1')

    tree.write(sddraft_file_path, encoding='UTF-8')

def apply_mapservice_permissions(parameters):
    (folder, service, service_type, user, password, serverName, serverPort, role, secure) = parameters
    token = getToken(user, password, serverName, serverPort, secure)
    make_folder_private(serverName, serverPort, token, folder)
    make_service_private(serverName, serverPort, token, folder + '/' + service, service_type)
    set_folder_permission(serverName, serverPort, token, folder, role)
    set_service_permission(serverName, serverPort, token, folder + '/' + service, service_type, role)

def publish_gpservice(ags_connection_path, working_directory, parameters):

    service_name = parameters[parameter_name_gpservice]
    sddraft = os.path.join(working_directory, service_name + '.sddraft')
    sddraft_backup = os.path.join(working_directory, service_name + '.backup')
    sd = os.path.join(working_directory, service_name + '.sd')

    # run the extract data task and assign it to the 'result' variable
    # only the cityhall layer was used as input, but the airport and firestation layers will be used in the service creation
    arcpy.ImportToolbox(os.path.abspath(os.path.join(current_path, '../scripts', "Toolbox.pyt")))

    # tool parameters
    serviceExecutionType = "Asynchronous"
    extent = "arcpy.Extent(0,0,0,0)"
    features = "arcpy.FeatureSet(parameters[parameter_name_example_featureclass])"
    result = arcpy.Tool(extent, features, features, features)

    '''
    # make sure the folder is registered with the server, if not, add it to the datastore
    if arcpy.env.workspace not in [i[2] for i in arcpy.ListDataStoreItems(ags_connection_path, 'FOLDER')]:
        # both the client and server paths are the same
        dsStatus = arcpy.AddDataStoreItem(ags_connection_path, "FOLDER", "CityData", arcpy.env.workspace, arcpy.env.workspace)
        print "Data store : " + str(dsStatus)
    '''

    arcpy.CreateGPSDDraft(
        result, sddraft, service_name, server_type="ARCGIS_SERVER",
        connection_file_path=ags_connection_path, copy_data_to_server=False,
        folder_name=parameters[parameter_name_folder], summary="Export SEP Service",
        tags="export, print, SEP, pdf", executionType=serviceExecutionType)
    shutil.copyfile(sddraft, sddraft_backup)

    # analyze the service definition draft
    analyzeMessages = arcpy.mapping.AnalyzeForSD(sddraft)

    # stage and upload the service if the sddraft analysis did not contain errors
    if analyzeMessages['errors'] == {}:
        # Execute StageService
        arcpy.StageService_server(sddraft, sd)
        # Execute UploadServiceDefinition
        upStatus = arcpy.UploadServiceDefinition_server(sd, ags_connection_path)
        print "Completed upload"
    else:
        # if the sddraft analysis contained errors, display them
        print analyzeMessages['errors']

def makeServicePublic(serverName, serverPort, token, service, serviceType):
    url = "/arcgis/admin/services/" + service + "." + serviceType + "/permissions/add"
    params = urllib.urlencode({'principal': 'esriEveryone', 'isAllowed': 'true', 'f': 'json', 'token': token})

    makeHttpPost(serverName, serverPort, url, params)
    print "Successfully made " + service + "." + serviceType + " public."

def make_folder_private(serverName, serverPort, token, folder):
    url = "/arcgis/admin/services/" + folder + "/permissions/add"
    apply_privacy(serverName, serverPort, token, url)

def make_service_private(serverName, serverPort, token, service, serviceType):
    url = "/arcgis/admin/services/" + service + "." + serviceType + "/permissions/add"
    apply_privacy(serverName, serverPort, token, url)

def apply_privacy(serverName, serverPort, token, url):
    params = urllib.urlencode({'principal': 'esriEveryone', 'isAllowed': 'false', 'f': 'json', 'token': token})
    makeHttpPost(serverName, serverPort, url, params)
    "Successfully made " + url + " private."


def set_folder_permission(serverName, serverPort, token, folder, role):
    url = "/arcgis/admin/services/" + folder + "/permissions/add"
    apply_permission(serverName, serverPort, token, url, role)

def set_service_permission(serverName, serverPort, token, service, serviceType, role):
    url = "/arcgis/admin/services/" + service + "." + serviceType + "/permissions/add"
    apply_permission(serverName, serverPort, token, url, role)

def apply_permission(serverName, serverPort, token, url, role):
    params = urllib.urlencode({'principal': role, 'isAllowed': 'true', 'f': 'json', 'token': token})
    makeHttpPost(serverName, serverPort, url, params)
    print "Successfully granted " + role + " permission to " + url


def getToken(username, password, serverName, serverPort, secure):
    tokenURL = "/arcgis/admin/generateToken"

    params = urllib.urlencode({'username': username, 'password': password, 'client': 'requestip', 'f': 'json'})

    response = makeHttpPost(serverName, serverPort, tokenURL, params, secure)
    tokenPattern = re.compile('[\w-]+')
    tokenMatch = tokenPattern.findall(response)[1]
    return tokenMatch


def makeHttpPost(serverName, serverPort, url, params, secure=False):
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    if (serverPort == 80):
        securePort = 443
    else:
        securePort = 6443

    try:
        if (secure):
            httpConn = httplib.HTTPSConnection(serverName, securePort)
        else:
            httpConn = httplib.HTTPConnection(serverName, serverPort)
        httpConn.request("POST", url, params, headers)
        response = httpConn.getresponse()
        if (response.status == 200):
            data = response.read()
            data_as_json_object = json.loads(data)
            if 'status' in data_as_json_object and data_as_json_object['status']=='error':
                raise Exception(data_as_json_object['messages'])
            httpConn.close()
            return data
        else:
            raise Exception(str(response))
    except Exception, e:
        print str(e)
        raise


